﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Request.Category
{
    public class CreateCategoryRequest
    {
        public CategoryDTO Category { get; set; }
    }
}