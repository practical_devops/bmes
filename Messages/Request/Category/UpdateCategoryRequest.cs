﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Request.Category
{
    public class UpdateCategoryRequest
    {
        public CategoryDTO Category { get; set; }
    }
}