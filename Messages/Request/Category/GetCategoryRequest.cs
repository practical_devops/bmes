﻿namespace BMES.API.Messages.Request.Category
{
    public class GetCategoryRequest
    {
        public int Id { get; set; }
    }
}