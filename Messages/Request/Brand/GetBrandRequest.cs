﻿namespace BMES.API.Messages.Request.Brand
{
    public class GetBrandRequest
    {
        public int Id { get; set; }
    }
}