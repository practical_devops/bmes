﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Request.Brand
{
    public class UpdateBrandRequest
    {
        public BrandDTO Brand { get; set; }
    }
}