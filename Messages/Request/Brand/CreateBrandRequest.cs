﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Request.Brand
{
    public class CreateBrandRequest
    {
        public BrandDTO Brand{ get; set; }
    }
}