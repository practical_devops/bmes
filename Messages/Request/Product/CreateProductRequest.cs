﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Request.Product
{
    public class CreateProductRequest
    {
        public ProductDTO Product { get; set; }
    }
}