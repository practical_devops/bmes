﻿namespace BMES.API.Messages.Request.Product
{
    public class FetchProductRequest
    {
        public int PageNumber { get; set; }
        public int ProductssPerPage { get; set; }
        public string CategorySlug { get; set; }
        public string BrandSlug { get; set; }
    }
}