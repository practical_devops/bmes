﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Request.Product
{
    public class UpdateProductRequest
    {
        public ProductDTO Product { get; set; }
    }
}