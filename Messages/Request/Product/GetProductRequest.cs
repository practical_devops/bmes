﻿namespace BMES.API.Messages.Request.Product
{
    public class GetProductRequest
    {
        public int Id { get; set; }
    }
}