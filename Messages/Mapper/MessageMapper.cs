﻿using BMES.API.Messages.DataTransferObjects.Product;
using BMES.API.Models.Product;

namespace BMES.API.Messages.Mapper
{
    public class MessageMapper
    {
        #region Map Brand

        public Brand MapToBrand(BrandDTO brandDto)
        {
            if (brandDto != null)
            {
                return new Brand
                {
                    Id = brandDto.Id,
                    Name = brandDto.BrandName,
                    Slug = brandDto.BrandSlug,
                    Description = brandDto.BrandDescription,
                    MetaDescription = brandDto.BrandMetaDescription,
                    MetaKeywords = brandDto.BrandMetaKeywords,
                    BrandStatus = (BrandStatus)brandDto.IsBrandActive,
                    ModifiedDate = brandDto.ModifiedDate,
                    IsDeleted = brandDto.IsDeleted
                };
            }

            return new Brand();
        }

        public BrandDTO MapToBrandDTO(Brand brand)
        {
            if (brand != null)
            {
                return new BrandDTO
                {
                    Id = brand.Id,
                    BrandName = brand.Name,
                    BrandSlug = brand.Slug,
                    BrandDescription = brand.Description,
                    BrandMetaDescription = brand.MetaDescription,
                    BrandMetaKeywords = brand.MetaKeywords,
                    IsBrandActive = (int)brand.BrandStatus,
                    ModifiedDate = brand.ModifiedDate,
                    IsDeleted = brand.IsDeleted
                };
            }

            return new BrandDTO();
        }

        #endregion

        #region Map Category

        public Category MapToCategory(CategoryDTO categoryDto)
        {
            if (categoryDto != null)
            {
                return new Category()
                {
                    Id = categoryDto.Id,
                    CategoryStatus = (CategoryStatus)categoryDto.IsCategoryActive,
                    Description = categoryDto.CategoryDescription,
                    MetaDescription = categoryDto.CategoryMetaDescription,
                    MetaKeywords = categoryDto.CategoryMetaKeywords,
                    Name = categoryDto.CategoryName,
                    Slug = categoryDto.CategorySlug,
                    CreatedDate = categoryDto.CreatedDate,
                    ModifiedDate = categoryDto.ModifiedDate,
                    IsDeleted = categoryDto.IsDeleted
                };
            }

            return new Category();
        }

        public CategoryDTO MapToCategoryDTO(Category category)
        {
            if (category != null)
            {
                return new CategoryDTO()
                {
                    Id = category.Id,
                    IsCategoryActive = (int)category.CategoryStatus,
                    CategoryDescription = category.Description,
                    CategoryMetaDescription = category.MetaDescription,
                    CategoryMetaKeywords = category.MetaKeywords,
                    CategoryName = category.Name,
                    CategorySlug = category.Slug,
                    CreatedDate = category.CreatedDate,
                    ModifiedDate = category.ModifiedDate,
                    IsDeleted = category.IsDeleted
                };
            }

            return new CategoryDTO();
        }

        #endregion

        #region Map Product

        public Product MapToProduct(ProductDTO productDto)
        {
            return new Product
            {
                Id = 
            };
        }

        #endregion
    }
}