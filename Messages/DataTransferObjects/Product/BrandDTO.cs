﻿using System;

namespace BMES.API.Messages.DataTransferObjects.Product
{
    public class BrandDTO
    {
        public long Id { get; set; }
        public string BrandName { get; set; }
        public string BrandSlug { get; set; }
        public string BrandDescription { get; set; }
        public string BrandMetaDescription { get; set; }
        public string BrandMetaKeywords { get; set; }
        public int IsBrandActive { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}