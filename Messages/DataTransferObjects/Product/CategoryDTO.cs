﻿using System;

namespace BMES.API.Messages.DataTransferObjects.Product
{
    public class CategoryDTO
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public string CategorySlug { get; set; }
        public string CategoryDescription { get; set; }
        public string CategoryMetaDescription { get; set; }
        public string CategoryMetaKeywords { get; set; }
        public int IsCategoryActive { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}