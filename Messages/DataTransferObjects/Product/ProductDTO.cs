﻿using System;

namespace BMES.API.Messages.DataTransferObjects.Product
{
    // The names in the DTO have been changed since we dont want someone interacting
    // with our DTO know what model we are using in our application
    public class ProductDTO
    {
        public long Id { get; set; }
        public string ProductName { get; set; }
        public string ProductSlug { get; set; }
        public string ProductMetDescription { get; set; }
        public string ProductMetaKeywords { get; set; }
        public string ProductSKU { get; set; }
        public string ProductModel { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal ProductSalePrice { get; set; }
        public decimal ProductOldPrice { get; set; }
        public string ProductImageUrl { get; set; }
        public int ProductQuantityInStock { get; set; }
        public bool ProductIsBestSeller { get; set; }
        public bool ProductIsFeatured { get; set; }
        public long ProductCategoryId { get; set; }
        public CategoryDTO ProductCategory { get; set; }
        public long ProductBrandId { get; set; }
        public BrandDTO ProductBrand { get; set; }
        public int IsProductActive { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}