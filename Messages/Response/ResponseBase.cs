﻿using System;
using System.Collections.Generic;
using System.Net;

namespace BMES.API.Messages.Response
{
    public class ResponseBase
    {
        public ResponseBase()
        {
            Messages = new List<String>();
        }

        public HttpStatusCode StatusCode { get; set; }
        public List<string> Messages { get; set; }
    }
}