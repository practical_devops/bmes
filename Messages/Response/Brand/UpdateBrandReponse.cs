﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Response.Brand
{
    public class UpdateBrandReponse : ResponseBase
    {
        public BrandDTO Brand { get; set; }
    }
}