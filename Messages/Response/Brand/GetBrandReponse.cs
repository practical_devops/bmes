﻿namespace BMES.API.Messages.Response.Brand
{
    public class GetBrandReponse : ResponseBase
    {
        public int Id { get; set; }
    }
}