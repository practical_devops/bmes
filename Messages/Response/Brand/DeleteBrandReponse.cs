﻿namespace BMES.API.Messages.Response.Brand
{
    public class DeleteBrandReponse : ResponseBase
    {
        public long Id { get; set; }
    }
}