﻿using BMES.API.Messages.DataTransferObjects.Product;

namespace BMES.API.Messages.Response.Brand
{
    public class CreateBrandResponse : ResponseBase
    {
        public BrandDTO Brand { get; set; }
    }
}