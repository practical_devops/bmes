﻿using BMES.API.Database;
using BMES.API.Models.Product;
using BMES.API.Repositories.Contracts;
using System.Collections.Generic;

namespace BMES.API.Repositories.Implementations
{
    public class BrandRepository: IBrandRepository
    {
        BmesDbContext _context;

        public BrandRepository(BmesDbContext context)
        {
            _context = context;
        }

        public void DeleteBrand(Brand brand)
        {
            _context.Remove(brand);
        }

        public Brand FindBrandById(long id)
        {
            return _context.Brands.Find(id);
        }

        public IEnumerable<Brand> GetAllBrands()
        {
            return _context.Brands;
        }

        public void SaveBrand(Brand brand)
        {
            _context.Brands.Add(brand);
            _context.SaveChanges();
        }

        public void UpdateBrand(Brand brand)
        {
            _context.Brands.Update(brand);
            _context.SaveChanges();
        }
    }
}