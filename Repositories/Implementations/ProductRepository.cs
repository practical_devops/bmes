﻿using BMES.API.Database;
using BMES.API.Models.Product;
using BMES.API.Repositories.Contracts;
using System.Collections.Generic;

namespace BMES.API.Repositories.Implementations
{
    public class ProductRepository : IProductRepository
    {
        BmesDbContext _context;

        public ProductRepository(BmesDbContext context)
        {
            _context = context;
        }

        public void DeleteProduct(Product product)
        {
            _context.Remove(product);
        }

        public Product FindProductById(long id)
        {
            return _context.Products.Find(id);
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _context.Products;
        }

        public void SaveProduct(Product product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();
        }

        public void UpdateProduct(Product product)
        {
            _context.Products.Update(product);
            _context.SaveChanges();
        }
    }
}