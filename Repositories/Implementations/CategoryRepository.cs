﻿using BMES.API.Database;
using BMES.API.Models.Product;
using BMES.API.Repositories.Contracts;
using System.Collections.Generic;

namespace BMES.API.Repositories.Implementations
{
    public class CategoryRepository : ICategoryRepository
    {
        BmesDbContext _context;

        public CategoryRepository(BmesDbContext context)
        {
            _context = context;
        }

        public void DeleteCategory(Category category)
        {
            _context.Remove(category);
        }

        public Category FindCategoryById(long id)
        {
            return _context.Categories.Find(id);
        }

        public IEnumerable<Category> GetAllCategories()
        {
            return _context.Categories;
        }

        public void SaveCategory(Category category)
        {
            _context.Categories.Add(category);
            _context.SaveChanges();
        }

        public void UpdateCategory(Category category)
        {
            _context.Categories.Update(category);
            _context.SaveChanges();
        }
    }
}