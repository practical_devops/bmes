﻿using BMES.API.Models.Product;
using System.Collections;
using System.Collections.Generic;

namespace BMES.API.Repositories.Contracts
{
    public interface IProductRepository
    {
        Product FindProductById(long id);
        IEnumerable<Product> GetAllProducts();
        void SaveProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);
    }
}