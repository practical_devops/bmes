﻿using BMES.API.Models.Product;
using System.Collections.Generic;

namespace BMES.API.Repositories.Contracts
{
    public interface ICategoryRepository
    {
        Category FindCategoryById(long id);
        IEnumerable<Category> GetAllCategories();
        void SaveCategory(Category product);
        void UpdateCategory(Category product);
        void DeleteCategory(Category product);
    }
}