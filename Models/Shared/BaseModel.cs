﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BMES.API.Models.Shared
{
    public class BaseModel
    {
        [Key]
        public long Id { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}