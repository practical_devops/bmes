﻿namespace BMES.API.Models.Product
{
    public enum BrandStatus
    {
        Active = 0,
        InActive = 1
    }
}