﻿namespace BMES.API.Models.Product
{
    public enum CategoryStatus
    {
        Active = 0,
        InActive = 1
    }
}